//
//  ContentView.swift
//  drawing WatchKit Extension
//
//  Created by Claudia Lamagna on 15/01/2020.
//  Copyright © 2020 Claudia Lamagna. All rights reserved.
//

import SwiftUI
import UIKit

struct HomeView: View {
    
    @State var count: Int = 21
    
    @State var bool: Bool = false
    
    @State var navigationBarBackButtonHidden = true
    
    @State var buttonEnabled = true
    @State var pillImage: String = "Pill"
    @State var timeRemaining = 30
    let timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
    
    @State var percent : CGFloat = 49
    func enableButton() {
        if self.timeRemaining > 0 {
            return self.buttonEnabled = false
        } else {
            return buttonEnabled = true
        }
    }
    
    var body: some View {
        
        VStack {
//            Loader(percent: percent)
            
//            Spacer()
            
            if self.buttonEnabled == true {
                Image("TookThePill2")
                    .renderingMode(.original)
                    .resizable()
                    .frame(width: WKInterfaceDevice.current().screenBounds.width * 0.9, height: WKInterfaceDevice.current().screenBounds.height * 0.3, alignment: .leading)
                    .multilineTextAlignment(.center)
                    .lineLimit(1)
//                    .frame(minWidth: 200, idealWidth: 50, maxWidth: 50, minHeight: 50, idealHeight: 50, maxHeight: 50, alignment: .center)
            }
            else  {
                Image("DoneScreen")
                .renderingMode(.original)
                .resizable()
                .frame(width: WKInterfaceDevice.current().screenBounds.width * 0.9, height: WKInterfaceDevice.current().screenBounds.height * 0.35, alignment: .leading)
            }
            Spacer(minLength: -5)
            VStack {
                Button(action: {
                    if(self.count != 0) {
                       self.count -= 1
                       self.percent -= 1.8
                       self.buttonEnabled = false
                    }
                    self.pillImage = "PillTaken"
                }) {
                    ZStack {
                        Image(pillImage)
                            .renderingMode(.original)
                            .resizable()
                            .overlay(CircularAnimation(timeRemaining: self.$timeRemaining))
                            .frame(width: WKInterfaceDevice.current().screenBounds.width * 0.25, height: WKInterfaceDevice.current().screenBounds.height * 0.2, alignment: .center)
                        }
                }.disabled(!buttonEnabled)
                .padding(.all, 7.0)
                .buttonStyle(PlainButtonStyle())
                    .onReceive(timer) { time in
                        if self.timeRemaining > 0 && self.buttonEnabled == false {
                            self.timeRemaining -= 1
                           if self.timeRemaining == 0 {
                            self.timeRemaining = 30
                            self.pillImage = "Pill"
                            self.buttonEnabled = true
                           }
                        }
                }
                
              Text(" \(count) / 21 Days")
              .bold()
            }.navigationBarHidden(true)
        }
        
        
    }
    
    struct CircularAnimation: View {
        @Binding var timeRemaining: Int
        var body: some View {
            ZStack {
//                Circle()
//                    .trim(from:0, to: 1.0)
//                    .stroke(Color.blue, lineWidth: 5)
//                    .opacity(0.3)
//                    .animation(Animation.linear(duration: Double(self.timeRemaining)).delay(1).repeatCount(1, autoreverses: false))
//                    .rotationEffect(Angle(degrees: 360))
                Circle()
                    .trim(from: 0, to: CGFloat(min(Double(self.timeRemaining)/30, 1.0)))
                    .stroke(Color.red, lineWidth: 5)
                    .foregroundColor(Color.blue)
                    .rotationEffect(.degrees(270))
                    .animation(.linear(duration: 1))
                        

               
            }
        }
    }
    
    struct Loader: View {
        
        var percent: CGFloat = 28
        var Colors: [Color] = [.yellow]
        
        var body: some View {
            ZStack {
                
                Rectangle()
                    .fill(Color.gray)
                    .frame(width: 150, height: 3)
                    .overlay(
                        
                        Rectangle()
                            .trim(from: 0, to: percent * 0.01)
                            .stroke(style: StrokeStyle(lineWidth: 8, lineCap: .square, lineJoin: .miter))
                            .fill(AngularGradient(gradient: .init(colors: Colors), center: .center)))
                //                .animation(.spring(response: 1.0, dampingFraction: 1.0, BlendDuration: 1.0))
                
                
                
                
                
            }
        }
    }
    func enableButton() -> Bool {
        if(self.buttonEnabled == true) {
            Timer.scheduledTimer(timeInterval: 5, target: self, selector: Selector(("enableButtn")), userInfo: nil, repeats: false)
            self.buttonEnabled = false
            return false
        } else {
            return true
        }
    }
    
    func enableButtn() {
        self.buttonEnabled = false
    }
    
}




struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}




