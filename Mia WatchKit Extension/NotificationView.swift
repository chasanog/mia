//
//  NotificationView.swift
//  MyFirstIndepApp WatchKit Extension
//
//  Created by cihan on 09/01/2020.
//  Copyright © 2020 cihanHasanoglu. All rights reserved.
//

import SwiftUI

struct NotificationView: View {
    var body: some View {
        Text("Did you take the Pill?")
    }
}

struct NotificationView_Previews: PreviewProvider {
    static var previews: some View {
        NotificationView()
    }
}
