//
//  SwiftUIView.swift
//  Mia WatchKit Extension
//
//  Created by cihan on 22/01/2020.
//  Copyright © 2020 cihanHasanoglu. All rights reserved.
//

import SwiftUI

struct SwiftUIView: View {
     @State var timeRemaining = 10
     let timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()

     var body: some View {
         Text("\(timeRemaining)")
             .onReceive(timer) { _ in
                 if self.timeRemaining > 0 {
                     self.timeRemaining -= 1
                    if self.timeRemaining == 0 {
                        self.timeRemaining = 10
                    }
                 }
             }
     }
}

struct SwiftUIView_Previews: PreviewProvider {
    static var previews: some View {
        SwiftUIView()
    }
}
