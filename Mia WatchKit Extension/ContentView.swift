//
//  ContentView.swift
//  MyFirstIndepApp WatchKit Extension
//
//  Created by cihan on 09/01/2020.
//  Copyright © 2020 cihanHasanoglu. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    @State var totalClicked: Int = 0
    var body: some View {
        VStack {
            GeometryReader { g in
                ZStack{
                    Text("Select your treatment:")
                        .font(.system(size: g.size.height > g.size.width ? g.size.width * 1: g.size.height * 0.8))
                        .bold()
                        .multilineTextAlignment(.center)
                }
            }
            
            VStack{
                NavigationLink(destination: HomeView()) {
                    Text("21 + 7")
                        .font(.headline)
                        
                        .foregroundColor(.yellow)
                    
                }
                .padding(.all, 5.0)
                
                NavigationLink(destination: CountDownView()) {
                    Text("28")
                        .font(.headline)
                        .foregroundColor(.yellow)
                        
                }.padding(.all, 5.0)
                
                        
            }.padding(.all, 10)
        }
        
     
    }
}

struct ContenView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
