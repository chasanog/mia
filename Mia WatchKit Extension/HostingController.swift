//
//  HostingController.swift
//  MyFirstIndepApp WatchKit Extension
//
//  Created by cihan on 09/01/2020.
//  Copyright © 2020 cihanHasanoglu. All rights reserved.
//

import WatchKit
import Foundation
import SwiftUI

class HostingController: WKHostingController<ContentView> {
    override var body: ContentView {
        return ContentView()
    }
}
