//
//  CountDownView.swift
//  Mia
//
//  Created by Claudia Lamagna on 19/01/2020.
//  Copyright © 2020 cihanHasanoglu. All rights reserved.
//

import SwiftUI

struct CountDownView: View {
    

    
    @State var percent : CGFloat = 49
    
    @State var count: Int = 28
    
    @State private var buttonEnabled = true
    
    @State private var timeRemaining = 10
    @State var pillImage: String = "Pill"
    
    let timer = Timer.publish(every: 1, on: .main, in: .common).autoconnect()
    
    var body: some View {
    
            VStack {
                
                Loader(percent: percent)
                Text(" \(count) /28")
                    .bold()
                Spacer()
                
                if self.buttonEnabled == true {
                    Text("Did you take the pill?")
                        .multilineTextAlignment(.center)
                        .lineLimit(1)
                        .frame(minWidth: 200, idealWidth: 50, maxWidth: 50, minHeight: 50, idealHeight: 50, maxHeight: 50, alignment: .center)
                }
                else  {
                    Text("You already took the pill!").frame(minWidth: 100, idealWidth: 100, maxWidth: 100, minHeight: 50, idealHeight: 50, maxHeight: 50, alignment: .center)
                }
                Spacer()
                HStack() {
                    Button(action: {
                        if(self.count != 0) {
                            self.count -= 1
                            self.percent -= 1.8
                            self.buttonEnabled = false
                            
                        }
                }){
                   Image(pillImage)
                    .renderingMode(.original)
                    .onReceive(timer) { _ in
                        if self.timeRemaining > 0 {
                            self.timeRemaining -= 1
                           if self.timeRemaining == 0 {
                            self.timeRemaining = 10
                            self.buttonEnabled = true
                           }
                        }
                    }
                }.disabled(!buttonEnabled)
                .padding(.all, 15)

                }.navigationBarHidden(true)
     }

  }
}
   

 



 struct Loader: View {
    
    var percent: CGFloat = 28
    var Colors: [Color] = [.yellow]
    
    var body: some View {
        ZStack {
            
            Rectangle()
                .fill(Color.gray)
                .frame(width: 150, height: 3)
                .overlay(
                    
            Rectangle()
                .trim(from: 0, to: percent * 0.01)
                .stroke(style: StrokeStyle(lineWidth: 8, lineCap: .square, lineJoin: .miter))
                .fill(AngularGradient(gradient: .init(colors: Colors), center: .center)))
//                .animation(.spring(response: 1.0, dampingFraction: 1.0, BlendDuration: 1.0))
                    
            
                
            
    
        }
    }
}

    
struct CountDownView_Previews: PreviewProvider {
    static var previews: some View {
        CountDownView()
    }
 }

